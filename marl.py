from prisonersdilemma import make_prisoners_dilemma_env
import ray
from ray import tune


if __name__ == '__main__':
    ray.init()
    PrisonsDilemma = make_prisoners_dilemma_env()
    # Example of common config: https://docs.ray.io/en/master/rllib-training.html#common-parameters
    # Example of PPO & GAE config: https://docs.ray.io/en/master/rllib-algorithms.html#ppo
    tune.run('PPO',
             stop={'timesteps_total': 500000},
             config={'env': PrisonsDilemma,
                     'env_config': {
                         'horizon': 100
                     },
                     'lr': 2e-5,
                     'lambda': 0.99,  # GAE parameter
                     'gamma': 0,  # discounting
                     'clip_param': 0.2,
                     'entropy_coeff': 0.2,
                     }
             )
