from ray.rllib.env.multi_agent_env import MultiAgentEnv
from gym import spaces
from gym import Env
import numpy as np


def make_prisoners_dilemma_env():
    class PrisonersDilemmaEnv(MultiAgentEnv):
        def __init__(self, config):
            self.agents = [
                Prisoner(config) for _ in range(2)
            ]
            self.dones = set()
            self.observation_space = self.agents[0].observation_space
            self.action_space = self.agents[0].action_space
            self.time = 0
            self.config = config

        def reset(self):
            self.dones = set()
            self.time = 0
            return {i: a.reset() for i, a in enumerate(self.agents)}

        def step(self, action_dict):
            self.time += 1
            obs, rew, done, info = {}, {}, {}, {}

            obs[0] = np.array([0])
            obs[1] = np.array([0])

            if action_dict[0] == 0 and action_dict[1] == 0:
                rew = {0: -3.,
                       1: -3.}
            elif action_dict[0] == 1 and action_dict[1] == 0:
                rew = {0: -5.,
                       1: 0.}
            elif action_dict[0] == 0 and action_dict[1] == 1:
                rew = {0: 0.,
                       1: -5.}
            else:  # both actions are 1
                rew = {0: -1.,
                       1: -1.}

            if self.time >= self.config['horizon']:
                done = {'__all__': True}
            else:
                done = {'__all__': False}

            return obs, rew, done, info

    return PrisonersDilemmaEnv


class Prisoner(Env):
    def __init__(self, config):
        self.obs = np.array([0.])
        self.counter = 0
        self.config = config
        self.observation_space = spaces.Box(-1, 1, (1, ))
        self.action_space = spaces.Discrete(2)

    def step(self, action):
        ...

    def reset(self):
        return self.obs

    def render(self, mode='human'):
        ...
